package hwo2014

import org.json4s._
import org.json4s.DefaultFormats


case class Car(id: Id, dimensions: Dimensions)
case class Dimensions(length: Double, width: Double, guideFlagPosition: Double)
case class RaceLane(distanceFromCenter: Int, index: Int)

case class Position(x: Double, y: Double)
case class StartingPoint(position: Position, angle: Double)
case class RaceSession(laps: Int, maxLapTimeMs: Int, quickRace: Boolean)

case class RaceData(race: Race)
case class Race(track: Track, cars: List[Car], raceSession: RaceSession)
case class Track(id: String, name: String, pieces: List[Piece], lanes: List[RaceLane], startingPoint: StartingPoint)

case class Piece(length: Double, angle: Double, switch: Boolean) {
  def this(length: Double) = this(length, 0.0, false)
  def this(length: Double, switch: Boolean) = this(length, 0.0, switch)
  def this(radius: Double, angle: Double) = this(radius, angle, false)

  def isBend() = angle != 0.0

  def getLength() = {
    if (isBend())
      2 * 3.14 * length * (angle / 360.0)
    else
      length
  }
}


case class Lane(startLaneIndex: Int, endLaneIndex: Int)
case class PiecePosition(pieceIndex: Int, inPieceDistance: Double, lane: Lane, lap: Int)
case class Id(name: String, color: String)
case class CarPosition(id: Id, angle: Double, piecePosition: PiecePosition)


case class Join(name: String, key: String)
case class MsgWrapper(msgType: String, data: JValue) {
}
object MsgWrapper {
  implicit val formats = new DefaultFormats{}

  def apply(msgType: String, data: Any): MsgWrapper = {
    MsgWrapper(msgType, Extraction.decompose(data))
  }
}
