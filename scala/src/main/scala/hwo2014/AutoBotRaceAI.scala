package hwo2014

trait RaceAI {
  def calculateNextMove(msg: MsgWrapper): MsgWrapper
  
}


class AutoBotRaceAI extends RaceAI {
   def calculateNextMove(msg: MsgWrapper): MsgWrapper = {
     msg match {
        case MsgWrapper("carPositions", _) =>
        case MsgWrapper(msgType, _) =>
          println("Received: " + msgType)
      }
     
     MsgWrapper("throttle", 0.5)
   }
}