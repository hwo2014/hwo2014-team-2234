package hwo2014

import org.json4s._
import org.json4s.DefaultFormats
import java.net.Socket
import java.io.{BufferedReader, InputStreamReader, OutputStreamWriter, PrintWriter}
import org.json4s.native.Serialization
import scala.annotation.tailrec

object AutoBotMain extends App {
  args.toList match {
    case hostName :: port :: botName :: botKey :: _ =>
      new AutoBot(hostName, Integer.parseInt(port), botName, botKey)
    case _ => println("args missing")
  }
}

class AutoBot(host: String, port: Int, botName: String, botKey: String) {
  implicit val formats = new DefaultFormats{}
  val socket = new Socket(host, port)
  val writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream, "UTF8"))
  val reader = new BufferedReader(new InputStreamReader(socket.getInputStream, "UTF8"))
  val raceAI:RaceAI = new AutoBotRaceAI 
    
  send(MsgWrapper("join", Join(botName, botKey)))
  play

  @tailrec private def play {
    val line = reader.readLine()
    if (line != null) {
      val msg= raceAI.calculateNextMove(Serialization.read[MsgWrapper](line))  
      send(msg)
      play
    }
  }

  def send(msg: MsgWrapper) {
    writer.println(Serialization.write(msg))
    writer.flush
  }
}



